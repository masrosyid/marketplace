package com.rosyid.marketplace.controller;

import com.rosyid.marketplace.dao.ProductDao;
import com.rosyid.marketplace.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/api/product")
public class ProductApiController {
    private static final Logger logger = LoggerFactory.getLogger(ProductApiController.class);

    @Autowired private ProductDao productDao;

    @GetMapping("/all")
    public Page<Product> findProducts(Pageable page){

        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product product){
        //Product product =  productDao.findOne(id); // tidak perlu lagi, karena sudah disediakan oleh Spring Data JPA
        logger.info("Mencari product dengan id {}", product.getId());
        return product;
    }

}
